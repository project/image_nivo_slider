/**
 * @file
 *  Applies the nivo slider functionality to the drupal blocks.
 */

(function ($) {
  Drupal.behaviors.imageNivoSlider = {
    attach: function (context, settings) {
      // Iterate over all nivo sliders available via the Drupal javascript settings.
      for (var key in Drupal.settings.image_nivo_slider) {
        var slider = Drupal.settings.image_nivo_slider[key];

        // Apply nivo slider to block
        $("#" + key + "-image-nivo-slider").nivoSlider({
          effect: slider.effect, //Specify sets like: 'fold,fade,sliceDown'
          slices: parseInt(slider.slices),
          boxCols: parseInt(slider.boxCols), // For box animations
          boxRows: parseInt(slider.boxRows),
          animSpeed: parseInt(slider.animSpeed), //Slide transition speed
          pauseTime: parseInt(slider.pauseTime),
          startSlide: parseInt(slider.startSlide), // Set starting Slide (0 index)
          directionNav: slider.directionNav, // Next & Prev navigation
          controlNav: slider.controlNav, // 1,2,3... navigation
          controlNavThumbs: slider.controlNavThumbs, // Use thumbnails for Control Nav
          pauseOnHover: slider.pauseOnHover, // Stop animation while hovering
          manualAdvance: slider.manualAdvance, // Force manual transitions
          captionOpacity: slider.captionOpacity, // Universal caption opacity
          prevText: slider.prevText, // Prev directionNav text
          nextText: slider.nextText, // Next directionNav text
          randomStart: slider.randomStart, // Start on a random slide
          beforeChange: new Function('', $.parseJSON(slider.beforeChange)), // Triggers before a slide transition
          afterChange: new Function('', $.parseJSON(slider.afterChange)), // Triggers after a slide transition
          slideshowEnd: new Function('', $.parseJSON(slider.slideshowEnd)), // Triggers after all slides have been shown
          lastSlide: new Function('', $.parseJSON(slider.lastSlide)), // Triggers when last slide is shown
          afterLoad: new Function('', $.parseJSON(slider.afterLoad)) // Triggers when slider has loaded
        });
      }
    }
  };

}(jQuery));
