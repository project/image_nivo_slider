/**
 * @file
 *  ?? Updates the Image Nivo Slider fieldset summary based on the status of the nivo slider block on the current media_gallery node. ??
 */

(function ($) {

Drupal.behaviors.imageNivoSliderForm = {
  attach: function (context) {

    // Toggle the box and slice specific fields based on the selected effect.
    // Once http://drupal.org/node/735528 is resolved in core handle this functionality with form states.
    Drupal.behaviors.imageNivoSliderForm.showOrHideEffectOptions($('#edit-image-nivo-slider-effect').val());
  

    $('#edit-image-nivo-slider-effect').change(function() {
      Drupal.behaviors.imageNivoSliderForm.showOrHideEffectOptions($(this).val());
    });
  },

  showOrHideEffectOptions: function(effect) {
    switch(effect) {
      // If the chosen effect is a slice effect show the slices field.
      case 'sliceDown':
      case 'sliceDownLeft':
      case 'sliceUp':
      case 'sliceUpLeft':
      case 'sliceUpDown':
      case 'sliceUpDownLeft':
        $('.form-item-slices').show();
        $('.form-item-box-cols').hide();
        $('.form-item-box-rows').hide();
        break;

      // If the chosen effect is a box effect show the box fields.
      case 'boxRandom':
      case 'boxRain':
      case 'boxRainReverse':
      case 'boxRainGrow':
      case 'boxRainGrowReverse':
        $('.form-item-box-cols').show();
        $('.form-item-box-rows').show();
        $('.form-item-slices').hide();
        break;

      default:
        $('.form-item-slices').hide();
        $('.form-item-box-cols').hide();
        $('.form-item-box-rows').hide();
        break;
    }
  }
};



})(jQuery);
