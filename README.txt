Image Nivo Slider

Overview:

This is another Nivo Slider module, heavily based on Media Nivo Slider, but this one provides its own content type for managing the images instead of requiring Media Gallery.

The module provides a new content type Image Set, which essentially just contains a multi-instance image field. An Image Set node, populated with images, automatically exposes the images as a Drupal block that runs a Nivo Slider slideshow. Drupal's standard block management can then be used to render the slideshow on the desired pages and location.

Multiple slider profiles can be configured, so that different slideshows can have different display styles and effects, etc. The images are displayed in the same order as in the Image Set node, where they can easily be re-ordered as required. The default widget for loading images into an Image Set is the Media Browser, which allows for selection of images already in use on the site, as well as uploading new images.

Dependencies:

Media
Libraries
Nivo Slider jQuery Plugin (2.7)

Installation:

Download and unpack the Image Nivo Slider module into your modules directory.

Download and unpack the module dependencies.
> Media http://drupal.org/project/media - Download the media gallery module and its dependencies and unpack it into your modules directory.
> Libraries Module http://drupal.org/project/libraries - Download the libraries module and unpack it into your modules directory.
> Nivo Slider jQuery plugin (2.7) http://nivo.dev7studios.com/ - Download the Nivo Slider jQuery plugin and unpack it to your libraries folder, typically 'sites/all/libraries'.

Enable the modules.
> Visit your site's Administration > Modules page.
> Enable Image Nivo Slider. This will automatically prompt you to approve enabling all required modules that are not already enabled.

Getting Started:

To get started with the Image Nivo Slider module you'll need to create an Image Set that will be the source of your slider images:

Go to Add content, and create an Image Set.
Click the Browse button below Attach media and upload and/or select library images to populate the slider.
The Nivo Slider will be follow the image ordering of the gallery so you can use drag-and-drop to rearrange the images to your liking.
Go to the Blocks administrative page.
Find the Image Nivo Slider block for your gallery and add it to a page region.
If required, configure the block to appear or be excluded from specific nodes.
View the slider by navigating to a page that contains your block.

To change the slideshow parameters, go to site configuration and use Image Nivo Slider in the Media section. The options are generally self-explanatory.
If you want more than one slideshow with different parameters, then create a new profile.
Then edit the Image Set, go to the Nivo Slider fieldset, and pick the required profile in the drop-down.

Notes:

Image Set nodes are by default not-published. This is because they are not meant to be viewed by site users, but are purely there as a configuration mechanism for Nivo Slider.

The default display of images in the Image Set is one per line, which even for maintenance is not very friendly. It can easily be improved by installing the Grid Field Formatter module.

Hint: to embed a slideshow in text, use the Insert Block module.

Acknowledgements:

This module is essentially a fork (AKA shameless copy!) of Media Nivo Slider by peronas.

I decided I liked Nivo Slider as a slideshow mechanism, and Media Nivo Slider did what I wanted in terms of image organisation, but I didn't like the overhead of Media Gallery (especially as I already used Node Gallery), in particular all the cruft extra fields that both Media Gallery and Media Nivo Slider added to every filetype. I don't need any of them and they just added confusion.

Hence this module with a custom content type for image organisation.
